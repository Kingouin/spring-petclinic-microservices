#!/bin/sh

restore_cache() {
  if [ -f "$CI_PROJECT_DIR/.m2/cache.tar" ]; then
    echo "Restoring cache..."
    tar -xf "$CI_PROJECT_DIR/.m2/cache.tar" -C "$CI_PROJECT_DIR"
  fi
}

save_cache() {
  echo "Saving cache..."
  tar -cf "$CI_PROJECT_DIR/.m2/cache.tar" -C "$CI_PROJECT_DIR" .m2/repository
}

case "$1" in
  restore)
    restore_cache
    ;;
  save)
    save_cache
    ;;
  *)
    echo "Invalid command. Usage: cache.sh [restore|save]"
    exit 1
    ;;
esac
